import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.Runnable;
import java.lang.Thread;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class Game extends JFrame implements Runnable
{

	public static int alpha = 0xFFFF00DC;
	private Canvas canvas = new Canvas();
	private RenderHandler renderer;
	//private BufferedImage testImage;
	//private Sprite testSprite;
	private SpriteSheet sheet;
	private Rectangle testRectangle = new Rectangle(0,0,50,50);
	private Tiles tiles;
	private Map map;
	private GameObject[] objects;
	private Player player;

	private KeyboardListener keyListener = new KeyboardListener();

	public Game(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0,0,1000,800);
		setLocationRelativeTo(null);

		add(canvas);
		setVisible(true);

		canvas.createBufferStrategy(3);
		renderer = new RenderHandler(getWidth(),getHeight());

		BufferedImage sheetImage = loadImage("Tiles1.png");
		sheet =  new SpriteSheet(sheetImage);
		sheet.loadSprites(16,16);

		tiles = new Tiles(new File("Tiles.txt"), sheet);

		map  = new Map(new File("Map.txt"),tiles);

		//testImage = loadImage("GrassTile.png");
		//testSprite  = sheet.getSprite(4,1);

		testRectangle.generateGraphics(3,0xFFCCDC);

		//load objects
		objects = new GameObject[1];
		player = new Player();
		objects[0] = player;

		//add listener
		canvas.addKeyListener(keyListener);
		canvas.addFocusListener(keyListener);

	}

	public void update(){
		for(int i=0;i<objects.length;i++)
			objects[i].update(this);

	}

	private BufferedImage loadImage(String path){
		try {
			BufferedImage loadedimage = ImageIO.read(Game.class.getResource(path));
			BufferedImage formattedImage = new BufferedImage(loadedimage.getWidth(),loadedimage.getHeight(),BufferedImage.TYPE_INT_RGB);
			formattedImage.getGraphics().drawImage(loadedimage,0,0,null);
			return formattedImage;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private int x=0;
	public void render(){
		BufferStrategy bufferStrategy = canvas.getBufferStrategy();
		bufferStrategy = canvas.getBufferStrategy();
		Graphics graphics = bufferStrategy.getDrawGraphics();
		super.paint(graphics);


		map.render(renderer,3,3);
		//renderer.renderRectangle(testRectangle,1,1);

		for(int i=0;i<objects.length;i++)
			objects[i].render( renderer,3,3);

		renderer.render(graphics);

		graphics.dispose();
		bufferStrategy.show();

		renderer.clear();
	}

	public RenderHandler getRenderer() {
		return renderer;
	}

	@Override
	public void run() {

		int i=0;
		int x=0;

		long lastTime = System.nanoTime();
		double nanoSecondConversion = 1000000000.0/60;
		double changeInSeconds = 0;
		while(true){
			long now = System.nanoTime();
			changeInSeconds += (now+lastTime) / nanoSecondConversion;
			while(changeInSeconds >= 1){
				update();
				changeInSeconds = 0;
			}
			render();
			lastTime=now;
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) 
	{
		Game game = new Game();
		Thread gameThread = new Thread(game);
		gameThread.start();
	}

	public KeyboardListener getKeyListener(){
		return keyListener;
	}


}