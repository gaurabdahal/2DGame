public interface GameObject {
    //call every time physically possible
    public void render(RenderHandler renderer, int xZoom, int yZoom);

    //call at 60fps rate
    public void update(Game game);
}
