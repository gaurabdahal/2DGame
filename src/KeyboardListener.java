import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardListener implements KeyListener,FocusListener{

    public boolean[] keys = new boolean[120];
    /**
     * Invoked when a key has been pressed.
     * See the class description for {@link KeyEvent} for a definition of
     * a key pressed event.
     *
     * @param event
     */
    @Override
    public void keyPressed(KeyEvent event) {
         int keyCode = event.getKeyCode();

         if(keyCode < keys.length)
                keys[keyCode] = true;

    }

    /**
     * Invoked when a key has been released.
     * See the class description for {@link KeyEvent} for a definition of
     * a key released event.
     *
     * @param event
     */
    @Override
    public void keyReleased(KeyEvent event) {
        int keyCode = event.getKeyCode();

        if(keyCode < keys.length)
            keys[keyCode] = false;
    }

    /**
     * Invoked when a component loses the keyboard focus.
     *
     * @param event
     */
    @Override
    public void focusLost(FocusEvent event) {
        for(int i=0;i<keys.length;i++){
            keys[i] = false;
        }

    }


    /**
     * Invoked when a key has been typed.
     * See the class description for {@link KeyEvent} for a definition of
     * a key typed event.
     *
     * @param e
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * Invoked when a component gains the keyboard focus.
     *
     * @param e
     */
    @Override
    public void focusGained(FocusEvent e) {

    }

    public boolean up(){
        return keys[KeyEvent.VK_W] || keys[KeyEvent.VK_UP];
    }

    public boolean down(){
        return keys[KeyEvent.VK_S] || keys[KeyEvent.VK_DOWN];
    }

    public boolean left(){
        return keys[KeyEvent.VK_A] || keys[KeyEvent.VK_LEFT];
    }

    public boolean right(){
        return keys[KeyEvent.VK_D] || keys[KeyEvent.VK_RIGHT];
    }




}
