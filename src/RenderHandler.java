import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;


public class RenderHandler 
{
	private BufferedImage view;
	private Rectangle camera;
	private int[] pixels;

	public RenderHandler(int width, int height) 
	{
		//Create a BufferedImage that will represent our view.
		view = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt) view.getRaster().getDataBuffer()).getData();
		camera = new Rectangle(0,0, width, height);

//		camera.x = -100;
//		camera.y = -30;
	}

	/**
	 * Draw view into the canvas
	 * @param graphics
	 */
	public void render(Graphics graphics)
	{
		graphics.drawImage(view, 0, 0, view.getWidth(), view.getHeight(), null);
	}

	//calculate the indices of pixels in canvas where image has to be copied
	public void renderImage(BufferedImage image, int xPosition, int yPosition, int xZoom, int yZoom) {
		int[] imagePixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		renderArray(imagePixels, image.getWidth(), image.getHeight(), xPosition, yPosition, xZoom, yZoom);
	}


	/**
	 * Read each pixel from sprite and draw into canvas
	 * @param sprite
	 * @param xPosition xPosition in canvas where we want to draw
	 * @param yPosition yPosition in canvas where we want to draw
	 * @param xZoom
	 * @param yZoom
	 */
	public void renderSprite(Sprite sprite,int xPosition, int yPosition, int xZoom, int yZoom){
		renderArray(sprite.getPixels(), sprite.getWidth(), sprite.getHeight(),xPosition, yPosition, xZoom, yZoom);
	}

	/**
	 * Draw rectangle into the canvas
	 * @param rectangle
	 * @param xZoom
	 * @param yZoom
	 */
	public void renderRectangle(Rectangle rectangle, int xZoom, int yZoom){
		int[] rectanglePixels = rectangle.getPixels();
		if(rectanglePixels != null)
			renderArray(rectanglePixels, rectangle.w, rectangle.h, rectangle.x, rectangle.y, xZoom, yZoom);
	}

	/**
	 * Draw array of pixels into the canvas
	 * @param renderPixels
	 * @param renderWidth
	 * @param renderHeight
	 * @param xPosition
	 * @param yPosition
	 * @param xZoom
	 * @param yZoom
	 */
	public void renderArray(int[] renderPixels, int renderWidth, int renderHeight, int xPosition, int yPosition, int xZoom, int yZoom){

		for (int y = 0; y < renderHeight; y++)
			for (int x = 0; x < renderWidth; x++)
				for (int yZoomPosition = 0; yZoomPosition < yZoom; yZoomPosition++)
					for (int xZoomPosition = 0; xZoomPosition < xZoom; xZoomPosition++)
						setPixel(renderPixels[y * renderWidth + x], ((x * xZoom) + xPosition + xZoomPosition), (y * yZoom) + yPosition + yZoomPosition);
	}

	//set the pixels of image into canvas
	private void setPixel(int pixel, int x, int y){
		if(x >= camera.x && y>= camera.y && x < camera.x + camera.w && y < camera.y + camera.h ) {
			int pixelIndex = (x - camera.x) + (y - camera.y) * view.getWidth();
			if (pixels.length > pixelIndex && pixel != Game.alpha)
				pixels[pixelIndex] = pixel;
		}
	}

	public Rectangle getCamera(){
		return camera;
	}


	public void clear(){
		for(int i=0; i<pixels.length;i++){
			pixels[i] = 0;
		}
	}


}