import java.awt.image.BufferedImage;

public class SpriteSheet {
    private int[] pixels;
    private BufferedImage image;
    public final int SIZEX;
    public final int SIZEY;
    private Sprite[] loadedSprites = null;
    private boolean spritesLoaded = false;
    private int spriteSizeX;

    public SpriteSheet(BufferedImage sheetImage){
        image = sheetImage;
        SIZEX = sheetImage.getWidth();
        SIZEY = sheetImage.getHeight();

        pixels = new int[SIZEX * SIZEY];
        pixels = sheetImage.getRGB(0,0,SIZEX,SIZEY,pixels,0,SIZEX);
    }


    /**
     * slice sprites from spritesheet and load into an array
     * @param spriteSizeX width of each sprite
     * @param spriteSizeY height of each sprite
     */


    public void loadSprites(int spriteSizeX, int spriteSizeY){
        this.spriteSizeX = spriteSizeX;
        loadedSprites = new Sprite[(SIZEX/spriteSizeX)*(SIZEY/spriteSizeY)];
        int spriteID=0;
        for(int y=0;y<SIZEY;y+=spriteSizeY){
            for(int x=0;x<SIZEX;x+=spriteSizeX){
                loadedSprites[spriteID] = new Sprite(this,x,y,spriteSizeX,spriteSizeY);
                spriteID++;
            }
        }
        spritesLoaded =true;
    }


    /**
     * Get sprites from a 2D array of sprites
     * @param x x-index of sprite in 2D array of sprites
     * @param y y-index of sprite in 2D array of sprites
     * @return sprite
     */
    public Sprite getSprite(int x, int y){
        if(spritesLoaded) {
            int spriteID = x + y * (SIZEX / this.spriteSizeX);
            if (spriteID < loadedSprites.length)
                return loadedSprites[spriteID];
            else
                System.out.println("SpriteID is not a valid index : " + spriteID);
        }else
            System.out.println("SpriteSheet not loaded yet");

        return null;

    }

    public int[] getPixels(){
        return pixels;
    }

    public BufferedImage getImage() {
        return image;
    }
}
